<?php

namespace App\Tests\Controller;

use App\Model\Library;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Response;

class PublishControllerTest extends WebTestCase {
    /**
     * Check that listen endpoint is working and sends mail
     */
    public function testListenAction() {
        $client = static::createClient();
        $client->enableProfiler();
        $client->catchExceptions(false);

        $hookBody = $this->getTestBookRequestBody();
        $headers = $this->getHeaders();
        $endpoint = '/admin/listen';

        $testBooksDir = $_ENV['BOOKS_DIR'];
        $testLibrary = new Library($testBooksDir);
        $container = self::$kernel->getContainer();
        $container->library = $testLibrary;

        $client->request('POST', $endpoint, [], [], $headers, $hookBody);
        $statusCode = $client->getResponse()->getStatusCode();

        $this->assertEquals(Response::HTTP_OK, $statusCode);

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        /** @var \Swift_Message[] $mails */
        $mails = $mailCollector->getMessages();
        $this->assertCount(1, $mails);

        $hookData = json_decode($hookBody, true);
        $sampleCommitHash = current($hookData['commits'])['id'];
        $sentMessage = array_shift($mails);

        $this->assertStringContainsString('Publishing Successful', $sentMessage->getBody());
        $this->assertStringContainsString($sampleCommitHash, $sentMessage->getBody());
    }

    /**
     * @return string
     */
    private function getTestBookRequestBody(): string {
        return file_get_contents(__DIR__ . '/../Files/webhook-gitlab-push-test-book.json');
    }

    /**
     * @return array
     */
    private function getHeaders(): array {
        $headers = [
            'HTTP_X-Gitlab-Event' => 'Push Hook', // prefix required for non-standard
            'Content-Type' => 'application/json'
        ];

        return $headers;
    }
}
