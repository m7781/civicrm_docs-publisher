<?php

namespace App\Model;

class LibraryTest extends \PHPUnit\Framework\TestCase {
    /**
     * @param string $identifier
     * @param array $expected
     * @dataProvider identifierProvider
     */
    public function testParseIdentifier($identifier, $expected) {
        $this->assertEquals($expected, Library::parseIdentifier($identifier));
    }

    public function identifierProvider() {
        return [
            [
                '',
                [
                    'bookSlug' => null,
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                '/dev',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                " /dev \n",
                [
                    'bookSlug' => 'dev',
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                "/foo bar/baz bat",
                [
                    'bookSlug' => 'foo bar',
                    'languageCode' => 'baz bat',
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                '/dev/en',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                '/dev/en/latest',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                'dev/en/latest',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                'dev/en/latest/',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                '//dev///////en//latest///',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page/',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => null,
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page#',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => null,
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page/#some-section',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => 'some-section',
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page/#some-section#another-section',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => 'some-section#another-section',
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page#some-section',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => 'some-section',
                ],
            ],
            [
                'dev/en/latest/category/foo/my-page.md#some-section',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page.md',
                    'fragment' => 'some-section',
                ],
            ],
            [
                'dev/#some-section',
                [
                    'bookSlug' => 'dev',
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => 'some-section',
                ],
            ],
        ];
    }
    /**
     * @param array $parts
     * @param string $expected
     *
     * @dataProvider identifierPartsProvider
     */
    public function testAssembleIdentifier($parts, $expected) {
        $this->assertEquals($expected, Library::assembleIdentifier($parts));
    }

    public function identifierPartsProvider() {
        return [
            [
                [
                    'bookSlug' => null,
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
                '',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
                'dev',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => null,
                ],
                'dev/en',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
                'dev/en/latest',
            ],
            [
                [
                    'bookSlug' => 'foo',
                    'languageCode' => 'bar',
                    'versionDescriptor' => 'baz',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => null,
                    'fragment' => null,
                ],
                'dev/en/latest',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => null,
                ],
                'dev/en/latest/category/foo/my-page',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => 'some-section',
                ],
                'dev/en/latest/category/foo/my-page/#some-section',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => 'en',
                    'versionDescriptor' => 'latest',
                    'editionIdentifier' => 'dev/en/latest',
                    'path' => 'category/foo/my-page',
                    'fragment' => 'some-section#another-section',
                ],
                'dev/en/latest/category/foo/my-page/#some-section#another-section',
            ],
            [
                [
                    'bookSlug' => 'dev',
                    'languageCode' => null,
                    'versionDescriptor' => null,
                    'editionIdentifier' => null,
                    'path' => null,
                    'fragment' => 'some-section',
                ],
                'dev',
            ],
        ];
    }
}
