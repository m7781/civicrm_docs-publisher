<?php

namespace App\Tests\Utils;

use App\Utils\WebhookAdapters\GithubHandler;
use App\Utils\WebhookAdapters\GitlabHandler;
use App\Utils\WebhookProcessor;
use Symfony\Component\HttpFoundation\Request;

class WebhookProcessorTest extends \PHPUnit\Framework\TestCase {

    /**
     * @param string $source
     * @param array $headers
     * @param string $body
     * @param array $emails
     * @param array $commitMsgs
     *
     * @dataProvider samplePushEventProvider
     */
    public function testSamplePushEvents($source, $headers, $body, $emails, $commitMsgs) {
        $handlers = [new GithubHandler(), new GitlabHandler()];
        $processor = new WebhookProcessor($handlers);

        $request = new Request([], [], [], [], [], [], $body);
        $request->headers->add($headers);

        $event = $processor->process($request);

        $foundEmails = $event->getNotificationRecipients();
        $foundMessages = $event->getCommitMessages();

        $this->assertEquals($source, $event->getSource());
        $this->assertEqualsCanonicalizing($emails, $foundEmails);
        $this->assertEqualsCanonicalizing($commitMsgs, $foundMessages);
    }

    public function samplePushEventProvider() {
        return [
            [
                'Github',
                ['X-Github-Event' => 'push'],
                file_get_contents(__DIR__ . '/../Files/webhook-github-push-sample.json'),
                [
                    'meusselea@fakedomain.com',
                    'means@fakedomain.com'
                ],
                [
                    'Tidy up hook_civicrm_buildForm Documentation and remove comments from wiki',
                    'Merge pull request #409 from meusselea/build_profile_doc_improvement',
                ]
            ],
            [
                'Gitlab',
                ['X-Gitlab-Event' => 'Push Hook'],
                file_get_contents(__DIR__ . '/../Files/webhook-gitlab-push-sample.json'),
                [
                    'jordi@softcatala.org',
                    'gitlabdev@dv6700.(none)'
                ],
                [
                    'Update Catalan translation to e38cb41.',
                    'fixed readme',
                ]
            ]
        ];
    }
}
