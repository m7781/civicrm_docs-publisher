<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Helper\Table;
use App\Model\Library;

class DocsListCommand extends Command {
    protected static $defaultName = 'docs:list';

    protected function configure() {
        $this
            ->setDescription('List the books available as yml files in the /books directory.')
        ;
    }

    public function __construct(Library $library) {
        $this->Library = $library;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        /** @var \App\Model\Library $library */
        $library = $this->Library;
        $table = new Table($output);
        $table->setHeaders(array('Book', 'Lang', 'Repo', 'Branch'));
        $table->addRows($library->booksAsTable());
        $table->render();
        return Command::SUCCESS;
    }
}
