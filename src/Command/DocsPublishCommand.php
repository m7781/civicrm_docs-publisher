<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Utils\Publisher;

class DocsPublishCommand extends Command {
    protected static $defaultName = 'docs:publish';

    protected function configure() {
        $this
            ->setDescription('Publish one or more books.')
            ->addArgument(
                'identifiers',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'One or more book identifiers (e.g. "user/en/master"). '
                . 'Partial identifiers are acceptable (e.g. "user/en" will publish all '
                . 'English versions of the User Guide. If no identifiers are '
                . 'specified, then all versions of all languages in all books will '
                . 'be published.',
            );
    }

    public function __construct(Publisher $publisher) {
        $this->Publisher = $publisher;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $identifiers = $input->getArgument('identifiers');
        if ($identifiers) {
            foreach ($identifiers as $identifier) {
                $this->Publisher->publish($identifier);
            }
        } else {
            $this->Publisher->publish();
        }
        foreach ($this->Publisher->getMessages() as $message) {
            $output->writeln($message['label'] . ': ' . $message['content']);
        }
        return Command::SUCCESS;
    }
}
