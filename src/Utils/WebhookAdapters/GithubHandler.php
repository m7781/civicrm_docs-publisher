<?php

namespace App\Utils\WebhookAdapters;

use App\Model\WebhookEvent;
use Symfony\Component\HttpFoundation\Request;

class GithubHandler implements WebhookHandler {
    /**
     * @inheritdoc
     */
    public function handle(Request $request): WebhookEvent {
        $event = new WebhookEvent();
        $event->setType($request->headers->get('X-Github-Event'));
        $event->setSource($event::SOURCE_GITHUB);
        $this->getDetailsFromPush($event, $request->getContent());

        return $event;
    }

    /**
     * @inheritdoc
     */
    public function canHandle(Request $request): bool {
        return $request->headers->has('X-Github-Event');
    }

    /**
     * Use a "push" event to figure out what branch and repo we are talking
     * about, and the also work out what emails we should send.
     *
     * @param WebhookEvent $event
     * @param string $payload
     *
     * @return WebhookEvent
     *
     * @throws \Exception
     */
    protected function getDetailsFromPush(WebhookEvent $event, $payload) {
        $payload = json_decode($payload);
        if (empty($payload)) {
            throw new \Exception('Could not decode webhook body');
        }

        $branch = basename($payload->ref);
        if (empty($branch)) {
            throw new \Exception("Unable to determine branch from payload data");
        }

        $repo = $payload->repository->html_url;
        if (empty($repo)) {
            throw new \Exception("Unable to determine repository from payload data");
        }

        $event->setRepo($repo);
        $event->setBranch($branch);
        $event->setCommits($payload->commits);

        return $event;
    }
}
