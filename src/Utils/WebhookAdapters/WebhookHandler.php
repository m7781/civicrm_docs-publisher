<?php

namespace App\Utils\WebhookAdapters;

use App\Model\WebhookEvent;
use Symfony\Component\HttpFoundation\Request;

interface WebhookHandler {
    const EVENT_PUSH = 'push';

    /**
     * Turns a request into a webhook event
     *
     * @param Request $request
     * @return WebhookEvent
     */
    public function handle(Request $request) : WebhookEvent;

    /**
     * Decides whether or not this adapter can handle the incoming request
     *
     * @param Request $request
     * @return bool
     */
    public function canHandle(Request $request) : bool;
}
