<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Library;

class ReadController extends AbstractController {
    /**
     * @Route("/")
     */
    public function homeAction(Library $library) {
        return $this->render(
            'read/home.html.twig',
            array(
                'core_books' => $library->getBooksByCategory('Core'),
                'extensions_books' => $library->getBooksByCategory('Extension'),
            )
        );
    }

    /**
     * Displays the default version for a book using the default language and version
     * configured in the books yml file.
     *
     * @route("/{slug}/", name="book", requirements={"slug": "(?!_)(?!admin/)[^/]+"})
     */
    public function bookAction($slug, Library $library) {
        /** @var \App\Model\Book */
        $book = $library->getBookBySlug($slug);
    
        if (!$book) {
            throw $this->createNotFoundException("We can't find a '$slug' book");
        }
            
        $language = $book->getDefaultLanguage();
        $version = $language->getDefaultVersion();
        return $this->redirect("/{$slug}/{$language->code}/{$version->path}");
    }

    /**
     * Displays a page for one book which shows the various languages and
     * versions available for the book.
     *
     * @route("/{slug}/editions", requirements={"slug": "(?!_)(?!admin/)[^/]+"},
     * name="book_editions")
     */
    public function editionsAction($slug, Library $library) {
        /** @var \App\Model\Book */
        $book = $library->getBookBySlug($slug);
            
        if (!$book) {
            throw $this->createNotFoundException("We can't find a '$slug' book");
        }
            
        return $this->render('read/book.html.twig', array('book' => $book));
    }

    /**
     * @route("/{slug}/{code}/", requirements={"slug": "(?!_)(?!admin/)[^/]+"})
     */
    public function languageAction($slug, $code, Library $library) {
        $path = "{$slug}/{$code}";
    
        $id = $library->getObjectsByIdentifier($path);
        $version = $id['language']->getDefaultVersion()->path;
            
        return $this->redirect("/{$path}/{$version}");
    }
}
