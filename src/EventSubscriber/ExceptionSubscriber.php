<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Flex\Response;
use App\Utils\Redirecter;

class ExceptionSubscriber implements EventSubscriberInterface {
    
    /**
    * @param \App\Utils\Redirecter $redirecter
    */
    public function __construct($redirecter) {
        $this->redirecter = $redirecter;
    }

    public function onExceptionEvent(ExceptionEvent $event) {
        $exception = $event->getThrowable();
        if ($exception instanceof NotFoundHttpException) {
            $requestUri = $event->getRequest()->getRequestUri();
            $redirect = $this->redirecter->lookupRedirect($requestUri);
            if ($redirect) {
                $response = new RedirectResponse($redirect);
                $event->setResponse($response);
            } else {
                throw new NotFoundHttpException(
                    'Sorry, the book you requested does not exist. Please check the URL and try again!'
                );
            }
        }
    }

    public static function getSubscribedEvents() {
        return [
            ExceptionEvent::class => 'onExceptionEvent',
        ];
    }
}
